package pl.edu.pjatk.s10888.nbd

import scala.math.BigDecimal.RoundingMode

object Ex1 extends App {

  val weekDays = List("Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota", "Niedziela")

  print("A: ")
  for (weekDay <- weekDays) print(s"$weekDay "); println()

  print("B: ")
  for (weekDay <- weekDays.filter(_.startsWith("P"))) print(s"$weekDay "); println()

  print("C: ")
  weekDays.foreach(d => print(s"$d ")); println()

  print("D: ")
  val iterator = weekDays.toIterator
  while(iterator.hasNext) print(s"${iterator.next()} "); println()

  print("E: ")
  def recPrint(list: List[String]): Unit = {
    if (list.nonEmpty) {
      print(s"${list.head} ")
      recPrint(list.tail)
    }
  }
  recPrint(weekDays); println()

  print("F: ")
  def recReversePrint(list: List[String]): Unit = {
    if (list.nonEmpty) {
      print(s"${list.last} ")
      recReversePrint(list.init)
    }
  }
  recReversePrint(weekDays); println()

  print("G1: ")
  println(weekDays.foldLeft("") { (acc, w) => s"$acc $w" }.trim)

  print("G2: ")
  println(weekDays.foldRight("") { (w, acc) => s"$w $acc" }.trim)

  print("H: ")
  weekDays.filter(_.startsWith("P")).foldLeft() { (_, w) => print(s"$w ") }; println()
}

object Ex2 extends App {

  def roundToTwoDecimals(number: Double) = BigDecimal(number).setScale(2, RoundingMode.HALF_UP).toDouble

  val products = Map(
    "Product 1" -> 99.99,
    "Product 2" -> 1000.00,
    "Product 3" -> 24.22
  )

  val discountedProducts = products.mapValues(price => roundToTwoDecimals(price * 0.9))

  println("Normal prices: ")
  products.foreach(e => println(s"${e._1}: ${e._2}"))
  println("Discounted prices: ")
  discountedProducts.foreach(e => println(s"${e._1}: ${e._2}"))
}

object Ex3 extends App {

  def printTriple(triple: (String, Int, Double)): Unit = println(s"${triple._1}, ${triple._2}, ${triple._3}")
  printTriple(("ABC", 100, 9.99))

}

object Ex4 extends App {

  val products = Map(
    "Product 1" -> 99.99,
    "Product 2" -> 1000.00,
    "Product 3" -> 24.22
  )

  val maybePriceForProduct1 = products.get("Product 1")
  val maybePriceForProduct4 = products.get("Product 4")

  def printOptionResults(product: String, price: Option[Double]): Unit = {
    price match {
      case Some(p) => println(s"Price for $product found, is: $p")
      case None => println(s"Price for $product not found!")
    }
  }

  printOptionResults("Product 1", maybePriceForProduct1)
  printOptionResults("Product 4", maybePriceForProduct4)
}

object Ex5 extends App {

  def checkDayType(weekDay: String) = weekDay match {
    case "Poniedzialek" | "Wtorek" | "Sroda" | "Czwartek" | "Piatek" => "Praca"
    case "Sobota" | "Niedziela" => "Weekend"
    case _ => "Nie ma takiego dnia"
  }

  List("Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota", "Niedziela", "Inny").foreach { w =>
    println(s"$w: ${checkDayType(w)}")
  }

}

object Ex6 extends App {

  class KontoBankowe(val stanKonta: Int) {

    def this() = this(0)

    def wplata(kwota: Int) = new KontoBankowe(stanKonta + kwota)
    def wyplata(kwota: Int) = new KontoBankowe(stanKonta - kwota)
  }

  private val konto = new KontoBankowe().wplata(1000).wplata(2000).wyplata(2500)
  println("Stan konta: " + konto.stanKonta)
}

object Ex7 extends App {

  case class Osoba(imie: String, nazwisko: String)

  def przywitaj(osoba: Osoba) = osoba match {
    case Osoba(imie, _) if imie.endsWith("a") => "Dzień dobry Pani!"
    case Osoba("Jaroslaw", "Kaczynski") => "Dzień dobry Panie Prezesie!"
    case Osoba(_, "Kowalski") => "Dzień dobry panie Kowalski!"
    case Osoba(imie, nazwisko) => s"Dzień dobry $imie $nazwisko!"
  }

  println(przywitaj(Osoba("Anna", "Nowak")))
  println(przywitaj(Osoba("Jan", "Kowalski")))
  println(przywitaj(Osoba("Jaroslaw", "Kaczynski")))
  println(przywitaj(Osoba("Magda", "Woźniak")))
  println(przywitaj(Osoba("Marcin", "Wrona")))

}

object Ex8 extends App {

  def filterOutZeroes(list: List[Int]) = list.filter(_ != 0)

  val list = List(2, 10, 0, 3, 9, 0, 0, 2, 4)
  println(s"Before filtering: $list")
  println(s"After filtering: ${filterOutZeroes(list)}")

}

object Ex9 extends App {

  def incrementList(list: List[Int]) = list.map(_ + 1)

  val list = List(2, 10, 0, 3, 9, 0, 0, 2, 4)
  println(s"Before incrementing: $list")
  println(s"After incrementing: ${incrementList(list)}")

}

object Ex10 extends App {

  def filterRelevantAndAbs(list: List[Double]) = list.filter(d => d >= -5 && d <= 12).map(math.abs)

  val list = List(6.4, -4.8, -6.2, 14, 12, -5, -2.1, 7.8)
  println(s"Before processing: $list")
  println(s"After processing: ${filterRelevantAndAbs(list)}")

}
